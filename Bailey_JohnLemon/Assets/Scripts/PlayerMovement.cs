﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public GameObject targetEnemy;
    public GameObject item;
    public Text inventoryText;
    public int inventory = 0;
    public GameObject dialogueHolder;
    public Text dialogueText;
    public bool dialogueOpened = false;
    public GameObject warningHolder;
    public Text warningText;
    public bool warningOpened = false;

    public GameEnding gameEnding;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        SetInventoryText();
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && targetEnemy != null) 
        {
            Destroy(targetEnemy);
        }
        if (Input.GetKeyDown(KeyCode.E) && dialogueOpened) 
        {
            ClosedialoguePanel();
        }
        if (Input.GetKeyDown(KeyCode.LeftShift) && item != null) 
        {
            Destroy(item);
            SetInventoryText();
        }
        if (inventory == 3)
        {
            gameEnding.EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("killzone")) 
        {
            targetEnemy = other.gameObject.transform.parent.gameObject;
        }
        if (other.gameObject.CompareTag("gargoyle")) 
        {
            Dialogue gDialogue = other.gameObject.GetComponent<Dialogue>();

            OpenDialoguePanel(gDialogue.dialogueContents);
        }
        if (other.gameObject.CompareTag("item")) 
        {
            item = other.gameObject.transform.parent.gameObject;
            inventory += 1;
        }
        if (other.gameObject.CompareTag("warningzone"))
        {
            warningHolder.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("killzone"))
        {
            if (other.gameObject.transform.parent.gameObject == targetEnemy) 
            {
                targetEnemy = null;
            }
        }
        if (other.gameObject.CompareTag("item"))
        {
            if (other.gameObject.transform.parent.gameObject == item)
            {
                item = null;

            }
        }
        if (other.gameObject.CompareTag("warningzone"))
        {
            warningHolder.SetActive(false);
        }

    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    void OpenDialoguePanel(string text)
    {
        dialogueOpened = true;

        //Time.timeScale = 0f;

        dialogueText.text = text;

        dialogueHolder.SetActive(true);
    }

    void ClosedialoguePanel() 
    {
        dialogueOpened = false;

        //Time.timeScale = 1f;

        dialogueHolder.SetActive(false);
    }
    void SetInventoryText()
    {
        inventoryText.text = "Inventory: " + inventory.ToString();
    }
}